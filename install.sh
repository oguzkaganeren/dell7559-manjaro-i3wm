#!/bin/bash
set -e
# Author 	: Oğuz Kağan EREN
# Website : https://www.oguzeren.org
#
#   DO NOT JUST RUN THIS. EXAMINE AND JUDGE. RUN AT YOUR OWN RISK.

#software from Manjaro Linux repositories

echo "################################################################"
echo "###################  Get fast mirrors  ################"
echo "################################################################"
sudo pacman-mirrors --fasttrack
echo -n "Do you want to update all packages (y/n)? "
read answer
if [ "$answer" != "${answer#[Yy]}" ] ;then
    sudo pacman -Syu
else
    sudo pacman -Syy
fi
sudo pacman -S --noconfirm --needed virtualgl lib32-virtualgl lib32-primus primus
sudo mhwd -i pci video-hybrid-intel-nvidia-390xx-bumblebee
sudo systemctl enable bumblebeed
sudo pacman -S --noconfirm --needed xorg-server xorg-apps xorg-xinit xorg-twm
sudo pacman -S --noconfirm --needed xf86-video-intel
sudo pacman -S --noconfirm --needed intel-ucode
sudo pacman -S --noconfirm --needed firefox
sudo pacman -S --noconfirm --needed git
sudo pacman -S --noconfirm --needed yaourt
sudo pacman -S --noconfirm --needed moka-icon-theme
sudo pacman -S --noconfirm --needed rofi
sudo pacman -S --noconfirm --needed compton
sudo pacman -S --noconfirm --needed pulseaudio pulseaudio-alsa pavucontrol --noconfirm --needed
sudo pacman -S --noconfirm --needed gst-plugins-good gst-plugins-bad gst-plugins-base gst-plugins-ugly gstreamer
pacmd list-cards
pacmd list-sinks
systemctl --user start pulseaudio
sudo pacman -S --noconfirm --needed xf86-video-fbdev aria2 git screenfetch ttf-ubuntu-font-family rxvt-unicode unace unrar zip unzip sharutils uudeview arj cabextract speedtest-cli
sudo pacman -S --noconfirm --needed ntp
sudo timedatectl set-ntp true
sudo pacman -S --noconfirm --needed deepin-movie
sudo pacman -S --noconfirm --needed virt-manager qemu vde2 ebtables dnsmasq bridge-utils openbsd-netcat
sudo systemctl enable libvirtd.service
sudo systemctl start libvirtd.service
sudo pacman -S --noconfirm --needed tlp tlp-rdw iw smartmontools ethtool x86_energy_perf_policy
sudo systemctl mask systemd-rfkill.socket systemd-rfkill.service
sudo pacman -S --noconfirm --needed lm_sensors && sudo sensors-detect
sudo pacman -S --noconfirm --needed thermald
sudo systemctl enable thermald
sudo systemctl start thermald
yaourt pikaur --noconfirm
pikaur -S --noconfirm --needed whatsapp-web-desktop
pikaur -S --noconfirm --needed materia-theme
pikaur -S --noconfirm --needed polybar-git
pikaur -S --noconfirm --needed opera chromium
pikaur -S --noconfirm --needed spotify-stable
pikaur -S --noconfirm --needed ttf-font-awesome ttf-font-awesome-4 powerline-fonts ttf-roboto  adobe-source-sans-pro-fonts android-studio woeusb-git visual-studio-code-bin

###############################################################################################

echo "################################################################"
echo "###################  Softwares installed  ################"
echo "################################################################"
